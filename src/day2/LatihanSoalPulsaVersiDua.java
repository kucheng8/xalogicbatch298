package day2;

import java.util.Scanner;

public class LatihanSoalPulsaVersiDua {
	
	public static void main(String[] args) {
		
		System.out.println("Silahkan masukkan jumlah pulsa yang anda beli(Idr): ");
		Scanner masukkanPulsa = new Scanner(System.in);
		int pulsa = masukkanPulsa.nextInt();
		
		int poin=0, poin1=0, poin2=0, poin3=0;
		int hitungPoin=0;
		if (pulsa<0) {
			System.out.println("Maaf, jumlah pulsa yang anda masukkan salah, terimakasih.");
		} else {
			if (pulsa<10001) {
				poin1=0;
				poin = poin1;
				System.out.println("Jumlah pulsa yang anda beli senilai Rp." + pulsa + ", point yang anda dapatkan sebesar " + poin + " point, terimakasih.");
				System.out.println("Detail Poin = 0 ");
			} else if (pulsa>=10001 && pulsa<30001) {
				poin2 = (pulsa-10000)/1000;
				poin = poin1 + poin2;
				System.out.println("Jumlah pulsa yang anda beli senilai Rp." + pulsa + ", point yang anda dapatkan sebesar " + poin + " point, terimakasih.");
				System.out.println("Detail Poin = 0 + " + poin2 + " = " + poin);
			} else if(pulsa>30000) {
				poin2 = (30000-10000)/1000;
				poin3 = ((pulsa-30000)/500);
				poin = poin1 + poin2 + poin3;
				System.out.println("Jumlah pulsa yang anda beli senilai Rp." + pulsa + ", point yang anda dapatkan sebesar " + poin + " point, terimakasih.");
				System.out.println("Detail Poin = 0 + " + poin2 + " + " + poin3 + " = " + poin);
			}
			
		}
	}

}
