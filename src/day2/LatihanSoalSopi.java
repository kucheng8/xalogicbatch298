package day2;

import java.util.Scanner;

public class LatihanSoalSopi {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Silahkan masukkan jumlah belanjaan anda(Idr): ");
		int jumlahBelanja = input.nextInt();
		System.out.println("Silahkan masukkan harga ongkos kirim(Idr): ");
		int ongkosKirim = input.nextInt();
		int totalBelanja;
		int potonganBelanja = 0;
		int potonganOngkir = 0;

		if (jumlahBelanja < 0 || ongkosKirim < 0) {
			System.out.println("Maaf, jumlah belanja atau ongkos kirim yang anda masukkan salah, terimakasih.");
		} else {
			if (jumlahBelanja < 30000) {
				potonganBelanja = 0;
				potonganOngkir = 0;
			} else if (jumlahBelanja >= 30000 && jumlahBelanja < 50000) {
				potonganBelanja = 5000;
				potonganOngkir = 5000;
			} else if (jumlahBelanja >= 50000 && jumlahBelanja < 100000) {
				potonganBelanja = 10000;
				potonganOngkir = 10000;
			} else if (jumlahBelanja >= 100000) {
				potonganBelanja = 20000;
				potonganOngkir = 10000;
			}
			totalBelanja = jumlahBelanja + ongkosKirim - potonganBelanja - potonganOngkir;
			System.out.println("Belanja: Rp. " + jumlahBelanja);
			System.out.println("Ongkos Kirim: Rp. " + ongkosKirim);
			System.out.println("Diskon Ongkir: Rp. " + potonganOngkir);
			System.out.println("Diskon Belanja: Rp. " + potonganBelanja);
			System.out.println("Total Belanja: Rp. " + totalBelanja);

		}
	}
}
