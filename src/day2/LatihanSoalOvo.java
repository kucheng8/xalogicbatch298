package day2;

import java.util.Scanner;

public class LatihanSoalOvo {
	public static void main(String[] args) {
		
		System.out.println("Silahkan masukkan jumlah belanjaan anda(Idr): ");
		Scanner input = new Scanner(System.in);
		int jumlahBelanja = input.nextInt();
		System.out.println("Silahkan masukkan jarak kirim(Km): ");
		int jarakKirim = input.nextInt();
		input.nextLine();
		System.out.println("Silahkan masukkan kode voucher: ");
		String voucher = input.nextLine();
		int totalBelanja;
		int hargaKirim = 0;
		int potonganVoucher=0;
		
		if (jarakKirim > 0 && jarakKirim <= 5) {
			hargaKirim = 5000;
		} else if (jarakKirim > 5) {
			hargaKirim = 5000 + ((jarakKirim - 5)*1000);
		}
		
		if (jumlahBelanja<0 || jarakKirim<0) {
			System.out.println("Maaf, jumlah belanja atau jarak kirim yang anda masukkan salah, terimakasih.");
		} else {
			if (voucher.equals("JKTOVO") && jumlahBelanja >= 30000 && jumlahBelanja<=75000) {
				 potonganVoucher = jumlahBelanja*40/100;
			} else if (voucher.equals("JKTOVO") && jumlahBelanja > 75000) {
				 potonganVoucher = 30000;
			}
			totalBelanja= jumlahBelanja + hargaKirim - potonganVoucher;
			System.out.println("________________________________________________________");
			System.out.println("Belanja			: Rp. " + jumlahBelanja);
			System.out.println("Diskon 40%		: Rp. " + potonganVoucher);
			System.out.println("Ongkos Kirim		: Rp. " + hargaKirim);
			System.out.println("Total Belanja		: Rp. " + totalBelanja);
			System.out.println("________________________________________________________");
			
		}
	}
}
