package day2;

import java.util.Scanner;

public class LatihanSoalNomor1 {
	public static void main(String[] args) {
		
		System.out.println("Silahkan masukkan jumlah pulsa yang anda beli(Idr): ");
		Scanner input = new Scanner(System.in);
		int jumlahPulsa = input.nextInt();
		
		int point=0;
		if (jumlahPulsa<0) {
			System.out.println("Maaf, jumlah pulsa yang anda masukkan salah, terimakasih.");
		} else {
			if (jumlahPulsa>0 && jumlahPulsa<10000) {
				point=0;
			} else if (jumlahPulsa>=10000 && jumlahPulsa<25000) {
				point=80;
			} else if (jumlahPulsa>=25000 && jumlahPulsa<50000) {
				point=200;
			} else if (jumlahPulsa>=50000 && jumlahPulsa<100000) {
				point=400;
			} else if (jumlahPulsa>=100000) {
				point=800;
			}
			System.out.println("Jumlah pulsa yang anda beli senilai Rp." + jumlahPulsa + ", point yang anda dapatkan sebesar " + point + " point, terimakasih.");
		}
	}
}
