package day9;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws ParseException {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {

		System.out.println("*Soal1 - Game Togel*");

		System.out.print("Masukkan poin awal: ");
		int poin = input.nextInt();
		String ulang = "y";

		while (ulang.toLowerCase().equals("y")) {

			System.out.print("Masukkan jumlah poin taruhan: ");
			int taruhan = input.nextInt();
			if (taruhan > poin) {
				System.out.println(
						"Maaf poin taruhan yang anda masukkan melebihi jumlah poin yang anda miliki, silahkan ulangi.");
				System.out.println();
				continue;
			}

			System.out.print("Silahkan masukkan tebakan anda, U untuk Up & D untuk Down: ");
			String tebakan = input.next().toUpperCase();
			int angka = (int) Math.random() * 10;
			System.out.println("Angka yang keluar adalah: " + angka);
			if (tebakan.equals("U") && angka > 5) {
				System.out.println("Selamat tebakan anda benar, poin akan ditambahkan sejumlah nilai taruhan!");
				poin += taruhan;
			} else if (tebakan.equals("D") && angka < 5) {
				System.out.println("Selamat tebakan anda benar, poin akan ditambahkan sejumlah nilai taruhan!");
				poin += taruhan;
			} else if (angka == 5) {
				System.out.println("Hasil Seri! tidak ada perubahan pada poin anda.");
			} else {
				System.out.println("Maaf tebakan anda salah, poin akan dikurangkan sejumlah nilai taruhan.");
				poin -= taruhan;

			}

			System.out.println("Poin Anda: " + poin);
			if (poin == 0) {
				System.out.println("Game Over. Ingin memulai kembali permainan? (Y untuk ya, N untuk tidak)");
				ulang = input.next().toLowerCase();
				if (ulang.toLowerCase().equals("y")) {
					System.out.print("Masukkan poin awal: ");
					poin = input.nextInt();
				} else {
					System.out.println("Terimakasih sudah bermain, see you later..");
					break;
				}
			} else {
				System.out.print("Lanjutkan? (Y untuk ya, N untuk tidak) ");
				ulang = input.next().toLowerCase();
				if (ulang.toLowerCase().equals("n")) {
					System.out.print("Terimakasih sudah bermain, see you later..");
				}
			}

		}

	}

	private static void soal2() {

		System.out.println("*Soal 2 - Buah Dalam Keranjang*");

		System.out.print("Masukkan jumlah keranjang: ");
		int jumlahKeranjang = input.nextInt();
		String[] buahDiKeranjangStr = new String[jumlahKeranjang];
		int[] buahDiKeranjang = new int[jumlahKeranjang];
		int total = 0;
		for (int i = 0; i < jumlahKeranjang; i++) {
			System.out.println("Masukkan jumlah buah di keranjang-" + (i + 1));
			buahDiKeranjangStr[i] = input.next();
			if (buahDiKeranjangStr[i].toLowerCase().equals("kosong")) {
				buahDiKeranjangStr[i] = "0";
				buahDiKeranjang[i] = Integer.parseInt(buahDiKeranjangStr[i]);
				total += buahDiKeranjang[i];
			} else {
				buahDiKeranjang[i] = Integer.parseInt(buahDiKeranjangStr[i]);
				total += buahDiKeranjang[i];
			}

		}

		System.out
				.println("Keranjang ke berapa yang akan dibawa ke pasar? (jika lebih dari 1, gunakan tanda koma (,) )");
		String[] keranjangPasar = input.next().split(",");
		for (int i = 0; i < keranjangPasar.length; i++) {
			total -= buahDiKeranjang[Integer.parseInt(keranjangPasar[i]) - 1];
		}

		System.out.println("Sisa buah = " + total);

	}

	private static void soal3() {

		System.out.println("*Soal 3 - jumlah kombinasi tempat duduk*");

		System.out.print("Masukkan jumlah orang: ");
		int x = input.nextInt();

		for (int i = x - 1; i > 0; i--) {
			x *= i;
		}

		System.out.println("Ada " + x + " cara");

	}

	private static void soal4() {

		System.out.println("*Soal 4 - Aturan Pemberian Baju*");

		System.out.print("Masukkan Jumlah Laki-Laki Dewasa: ");
		int lelaki = input.nextInt();
		System.out.print("Masukkan Jumlah Wanita Dewasa: ");
		int wanita = input.nextInt();
		System.out.print("Masukkan Jumlah Anak: ");
		int anak = input.nextInt();
		System.out.print("Masukkan Jumlah Bayi: ");
		int bayi = input.nextInt();

		int total = lelaki + (wanita * 2) + (anak * 3) + (bayi * 5);

		if (total % 2 == 1 && total > 10) {
			total += wanita;
			System.out.println(total + " baju");
		} else {
			System.out.println(total + " baju");
		}

	}

	private static void soal5() {

		System.out.println("*Soal 5 - Perhitungan bahan yang diperlukan untuk jumlah pukis yang diinginkan*");

		System.out.print("Masukkan jumlah kue pukis yang akan dibuat: ");
		int jumlahKue = input.nextInt();
		float tepung = jumlahKue * 125f / 15f;
		float gula = jumlahKue * 100f / 15f;
		float susu = jumlahKue * 100f / 15f;
		float telur = jumlahKue * 100f / 15f;
		DecimalFormat df = new DecimalFormat("#0.000");
		System.out.println("Tepung Terigu: " + df.format(tepung) + " gram");
		System.out.println("Gula Pasir: " + df.format(gula) + " gram");
		System.out.println("Susu Murni: " + df.format(susu) + " gram");
		System.out.println("Putih Telur Ayam: " + df.format(telur) + " gram");

	}

	private static void soal6() throws ParseException {

		System.out.println("*Soal 6 - Perhitungan Parkir*");

		System.out.print("Silahkan masukkan waktu datang (dd/MM/yyyy HH.mm): "); // 20/08/2019 07.50
		String jamDatang = input.nextLine();
		System.out.print("Silahkan masukkan waktu pergi (dd/MM/yyyy HH.mm): "); // 20/08/2019 17.30
		String jamPergi = input.nextLine();

		String[] jam = new String[2];
		jam[0] = jamDatang;
		jam[1] = jamPergi;
		int salah = 0;

		for (int i = 0; i < jam.length; i++) {
			for (int j = 0; j < jam[i].length(); j++) {

				if ((int) jam[i].charAt(j) >= 46 && (int) jam[i].charAt(j) <= 57) {
				} else if ((int) jam[i].charAt(j) == 32) {
				} else {
					salah++;
				}

			}

			if (jam[i].length() != 16) {
				salah++;
			}

			int menit = Integer.parseInt(jam[i].substring(14, 16));
			int jamInt = Integer.parseInt(jam[i].substring(11, 13));
			int tanggal = Integer.parseInt(jam[i].substring(0, 2));
			int bulan = Integer.parseInt(jam[i].substring(3, 5));
			int tahun = Integer.parseInt(jam[i].substring(6, 10));

			if (menit < 0 || menit > 59) {
				salah++;
			} else if (jamInt < 0 || jamInt > 23) {
				salah++;
			} else if (bulan < 0 || bulan > 12) {
				salah++;
			} else if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10
					|| bulan == 12) {
				if (tanggal < 0 || tanggal > 31) {
					salah++;
				}
			} else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {
				if (tanggal < 0 || tanggal > 30) {
					salah++;
				}
			} else if (tahun % 4 == 0 && bulan == 2) {
				if (tanggal < 0 || tanggal > 29) {
					salah++;
				}
			} else if (tahun % 4 != 0 && bulan == 2) {
				if (tanggal < 0 || tanggal > 28) {
					salah++;
				}
			} else if (tahun > 2022) {
				salah++;
			}

		}

		if (salah > 0) {
			System.out.println("Maaf, inputan anda salah.");
			return;
		}

		Date waktuDatang = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(jamDatang);
		Date waktuPergi = new SimpleDateFormat("dd/MM/yyyy HH.mm").parse(jamPergi);

		long compareInMiliSecond = waktuPergi.getTime() - waktuDatang.getTime();
		float compareInHours = compareInMiliSecond / (3600f * 1000f);
		double compareInHoursdb = Math.ceil((double) compareInHours);
		int harga = (int) compareInHoursdb * 3000;

		System.out.println("Durasi parkir adalah " + (int) compareInHoursdb + " jam");
		System.out.println("Harga parkir yang harus dibayar adalah Rp." + harga);

	}

	private static void soal7() throws ParseException {

		System.out.println("*Soal 7 - Perhitungan Peminjaman Buku*");

		System.out.print("Silahkan masukkan waktu datang (dd-MM-yyyy): "); // 09-06-2019
		String tanggalP1 = input.nextLine();
		System.out.print("Silahkan masukkan waktu pergi (dd-MM-yyyy): "); // 10-07-2019
		String tanggalP2 = input.nextLine();

		String[] tanggal = new String[2];
		tanggal[0] = tanggalP1;
		tanggal[1] = tanggalP2;
		int salah = 0;

		for (int i = 0; i < tanggal.length; i++) {
			for (int j = 0; j < tanggal[i].length(); j++) {

				if ((int) tanggal[i].charAt(j) >= 48 && (int) tanggal[i].charAt(j) <= 57) {
				} else if ((int) tanggal[i].charAt(j) == 45) {
				} else {
					salah++;
				}
			}

			if (tanggal[i].length() != 10) {
				salah++;
			}
			int tanggalInt = Integer.parseInt(tanggal[i].substring(0, 2));
			int bulan = Integer.parseInt(tanggal[i].substring(3, 5));
			int tahun = Integer.parseInt(tanggal[i].substring(6, 10));

			if (bulan < 0 || bulan > 12) {
				salah++;
			} else if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10
					|| bulan == 12) {
				if (tanggalInt < 0 || tanggalInt > 31) {
					salah++;
				}
			} else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {
				if (tanggalInt < 0 || tanggalInt > 30) {
					salah++;
				}
			} else if (tahun % 4 == 0 && bulan == 2) {
				if (tanggalInt < 0 || tanggalInt > 29) {
					salah++;
				}
			} else if (tahun % 4 != 0 && bulan == 2) {
				if (tanggalInt < 0 || tanggalInt > 28) {
					salah++;
				}
			} else if (tahun > 2022) {
				salah++;
			}
		}

		if (salah > 0) {
			System.out.println("Maaf, inputan anda salah.");
			return;
		}

		Date tanggalPeminjaman = new SimpleDateFormat("dd-MM-yyyy").parse(tanggalP1);
		Date tanggalPengembalian = new SimpleDateFormat("dd-MM-yyyy").parse(tanggalP2);

		long gapInMiliSecond = tanggalPengembalian.getTime() - tanggalPeminjaman.getTime();
		float gapInDays = gapInMiliSecond / (3600f * 1000f * 24f);
		double gapInDaysDb = Math.ceil((double) gapInDays);
		if (gapInDaysDb > 3) {
			int denda = ((int) gapInDaysDb - 3) * 500;
			System.out.println("Lama keterlambatan pengembalian adalah " + ((int) gapInDaysDb - 3) + " hari");
			System.out.println("Jumlah denda yang harus dibayar adalah Rp." + denda);
		} else {
			System.out.println("Terimakasih karena anda telah mengembalikan buku sebelum terlambat");
		}

	}

	private static void soal8() {

		System.out.println("*Soal 8 - Palindrom*");

		System.out.print("Masukkan jumlah kata yang akan dicek: ");
		int jumlah = input.nextInt();
		String[] kata = new String[jumlah];
		String[] kataBalik = new String[jumlah];

		for (int i = 0; i < jumlah; i++) {
			System.out.print("Masukkan kata ke-" + (i + 1) + ": ");
			kata[i] = input.next().toLowerCase();
		}
		
		int salah = 0;

		for (int i = 0; i < jumlah; i++) {
			
			kataBalik[i] = "";

			for (int j = kata[i].length() - 1; j >= 0; j--) {
				if ((int) kata[i].charAt(j) < 97 && (int) kata[i].charAt(j) > 122) {
					System.out.println("Maaf, inputan anda salah.");
					continue;
				} 
				kataBalik[i] += kata[i].charAt(j);
			}
			

			if (kata[i].equals(kataBalik[i])) {
				System.out.println(kata[i] + " Yes");
			} else if (kata[i].equals("bakso") || kata[i].equals("sop")) {
				System.out.println("tumpah!!!!!!!!!");
			} else {
				System.out.println(kata[i] + " No");
			}

		}

	}

}
