package Day15_Pretest;

import java.text.ParseException;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {

		System.out.println("*Soal1 - Angka Ganjil Genap*");

		System.out.print("Masukkan n: ");
		int n = input.nextInt();

		int[][] larik = new int[2][n / 2];

		int x = 1;
		for (int i = 0; i < (n / 2); i++) {
			for (int j = 0; j < 2; j++) {

				larik[j][i] = x;
				x++;

			}
		}

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < (n / 2); j++) {

				if (j == (n / 2) - 1) {
					System.out.print(larik[i][j]);
				} else {
					System.out.print(larik[i][j] + " ");
				}

			}
			System.out.println();
		}

	}

	private static void soal2() {

		System.out.println("*Soal2 - Pemisahan Vokal dan Konsonan*");

		System.out.println("Masukkan Kalimat yang akan dipisahkan: ");
		String kalimat = input.nextLine();
		char[] kalimatGabungan = (kalimat.replaceAll(" ", "")).toLowerCase().toCharArray();
		String vokal = "";
		String konsonan = "";
		for (int i = 0; i < kalimatGabungan.length; i++) {

			if (kalimatGabungan[i] == 'a' || kalimatGabungan[i] == 'i' || kalimatGabungan[i] == 'u'
					|| kalimatGabungan[i] == 'e' || kalimatGabungan[i] == 'o') {
				vokal += kalimatGabungan[i];
			} else {
				konsonan += kalimatGabungan[i];
			}
		}

		char[] vokalArr = vokal.toCharArray();
		char[] konsonanArr = konsonan.toCharArray();

//		sorting vokal
		char temp = 0;
		for (int i = 0; i < vokalArr.length; i++) {
			for (int j = 0; j < vokalArr.length; j++) {
				if ((int) vokalArr[i] <= (int) vokalArr[j]) {
					temp = vokalArr[i];
					vokalArr[i] = vokalArr[j];
					vokalArr[j] = temp;
				}
			}
		}

		for (int i = 0; i < konsonanArr.length; i++) {
			for (int j = 0; j < konsonanArr.length; j++) {
				if ((int) konsonanArr[i] <= (int) konsonanArr[j]) {
					temp = konsonanArr[i];
					konsonanArr[i] = konsonanArr[j];
					konsonanArr[j] = temp;
				}
			}
		}

		System.out.println("Vokal    : " + String.valueOf(vokalArr));
		System.out.println("Konsonan : " + String.valueOf(konsonanArr));

	}

	private static void soal3() {

		System.out.println("*Soal3 - Aku Adalah Si Angka 1*");

		System.out.print("Masukkan n: ");
		int n = input.nextInt();
		char kutip = '"';
		int nilaiAwal = 100;

		for (int i = 0; i < n; i++) {
			nilaiAwal += 3 * i;
			System.out.println(nilaiAwal + " adalah " + kutip + "Si Angka 1" + kutip);
		}

	}

	private static void soal4() {

		System.out.println("         *Soal 4 - Konsumsi Bensin*           ");
		System.out.println("_________________________________________________");
		System.out.println("jalur 1. Jarak dari Toko ke Tempat 1       = 2.0 KM");
		System.out.println("jalur 2. Jarak dari Tempat 1 ke Tempat 2 = 0.5 KM");
		System.out.println("jalur 3. Jarak dari Tempat 2 ke Tempat 3 = 1.5 KM");
		System.out.println("jalur 4. Jarak dari Tempat 3 ke Tempat 4 = 2.5 KM");
		System.out.println("Masukkan jalur yang ditempuh oleh driver (pisahkan dengan strip(-) )");
		String masukan = input.nextLine().toLowerCase();

		String[] masukanStr = masukan.split("-");
		int[] masukanInt = new int[masukanStr.length];
		double jarakTempuh = 0;
		double rasioBensin = 2.5;

		double[][] jarak = { { 0.0, 2.0, 2.5, 4.0, 6.5 }, { 2.0, 0.0, 0.5, 2.0, 4.5 }, { 2.5, 0.5, 0.0, 1.5, 4.0 },
				{ 4.0, 2.0, 1.5, 0.0, 2.5 }, { 6.5, 4.5, 4.0, 2.5, 0.0 } };

		String hasil = "Jarak Tempuh = ";

		for (int i = 0; i < masukanStr.length; i++) {

			if (masukanStr[i].contains("toko")) {
				masukanInt[i] = 0;
			} else if (masukanStr[i].contains("1")) {
				masukanInt[i] = 1;
			} else if (masukanStr[i].contains("2")) {
				masukanInt[i] = 2;
			} else if (masukanStr[i].contains("3")) {
				masukanInt[i] = 3;
			} else if (masukanStr[i].contains("4")) {
				masukanInt[i] = 4;
			} else {
				System.out.println("maaf, jalur yang anda masukkan tidak tersedia.");
				return;
			}

		}

		for (int j = 0; j < masukanInt.length - 1; j++) {
			jarakTempuh += jarak[masukanInt[j]][masukanInt[j + 1]];
			hasil = hasil + jarak[masukanInt[j]][masukanInt[j + 1]] + " KM + ";
		}

		double konsumsiBensin = Math.ceil(jarakTempuh / rasioBensin);
		hasil = hasil.substring(0, hasil.length() - 2);

		System.out.println(hasil + "= " + jarakTempuh + " KM");
		System.out.println("Bensin = " + (int) konsumsiBensin + " Liter");

	}

	private static void soal5() {

		System.out.println("*Soal5 - Porsi Makanan*");

		System.out.print("Masukkan Jumlah Laki-Laki Dewasa: ");
		double lelaki = input.nextInt();
		System.out.print("Masukkan Jumlah Perempuan Dewasa: ");
		double perempuan = input.nextInt();
		System.out.print("Masukkan Jumlah Remaja: ");
		double remaja = input.nextInt();
		System.out.print("Masukkan Jumlah Anak: ");
		double anak = input.nextInt();
		System.out.print("Masukkan Jumlah Balita: ");
		double balita = input.nextInt();

		double jumlahOrang = lelaki + perempuan + remaja + anak + balita;

		double total = (lelaki * 2) + perempuan + remaja + (anak / 2) + balita;

		if (jumlahOrang % 2 == 1 && jumlahOrang > 5) {
			total += perempuan;
			System.out.println(total + " porsi");
		} else {
			System.out.println(total + " porsi");
		}

	}

	private static void soal6() {

		System.out.println("*Soal6 - Simulasi Transfer*");

		System.out.print("Masukkan PIN: ");
		int pin = input.nextInt();
		int pinAsli = 123456;
		if (pin != pinAsli) {
			System.out.println("PIN salah");
			return;
		}
		System.out.print("Jumlah uang yang disetor: ");
		long setoran = input.nextInt();

		System.out.print("Pilihan Transfer (1. Antar Rekening     2. Antar Bank): ");
		int pilihanTf = input.nextInt();

		if (pilihanTf == 1) {

			System.out.print("Masukkan rekening tujuan: ");
			long norek = input.nextLong();
			System.out.print("Masukkan nominal transfer: ");
			long nominalTf = input.nextLong();
			if (norek <= 999999999) {
				System.out.println("No Rekening salah.");
				return;
			} else if (nominalTf > setoran) {
				System.out.println("Saldo tidak mencukupi.");
				return;
			} else {
				long saldoAkhir = setoran - nominalTf;
				System.out.println("Transaksi berhasil, saldo saat ini Rp. " + saldoAkhir);
			}

		} else if (pilihanTf == 2) {

			System.out.print("Masukkan kode bank: ");
			int kodeBank = input.nextInt();
			System.out.print("Masukkan rekening tujuan: ");
			long norek = input.nextLong();
			System.out.print("Masukkan nominal transfer: ");
			long nominalTf = input.nextLong();
			if (norek <= 999999999) {
				System.out.println("No Rekening salah.");
				return;
			} else if (nominalTf > setoran) {
				System.out.println("Saldo tidak mencukupi.");
				return;
			} else {
				long saldoAkhir = setoran - nominalTf - 7500;
				System.out.println("Transaksi berhasil, saldo saat ini Rp. " + saldoAkhir);
			}

		} else {
			System.out.println("Silahkan coba kembali");
		}

	}

	private static void soal7() {

		System.out.println("*Soal7 - Game Tebak Gambar*");

		System.out.print("Masukkan jumlah kartu yang kamu miliki: ");
		int kartu = input.nextInt();
		String ulang = "y";

		while (ulang.toLowerCase().equals("y")) {

			System.out.print("Masukkan jumlah kartu tawaran: ");
			int tawaran = input.nextInt();
			if (tawaran > kartu) {
				System.out.println(
						"Maaf kartu tawaran yang kamu masukkan melebihi jumlah kartu yang kamu miliki, ulangi ya.");
				System.out.println();
				continue;
			}

			System.out.print("Silahkan masukkan pilihan kamu, A untuk kotak A & B untuk kotak B: ");
			String tebakan = input.next().toUpperCase();
			double angkaA = 5;
			double angkaB = (Math.random() * 10);
			if (tebakan.equals("A") && angkaA > angkaB) {
				System.out.println("Selamat tebakan kamu benar, kartu akan ditambahkan sejumlah nilai tawaran!");
				kartu += tawaran;
			} else if (tebakan.equals("B") && angkaA < angkaB) {
				System.out.println("Selamat tebakan kamu benar, kartu akan ditambahkan sejumlah nilai tawaran!");
				kartu += tawaran;
			} else if (angkaA == angkaB) {
				System.out.println("Hasil Seri! tidak ada perubahan pada jumlah kartu kamu.");
			} else {
				System.out.println("Maaf tebakan kamu salah, kartu akan dikurangkan sejumlah nilai tawaran.");
				kartu -= tawaran;

			}

			System.out.println("kartu kamu: " + kartu);
			if (kartu == 0) {
				System.out.println("Game Over. Ingin memulai kembali permainan? (Y untuk ya, N untuk tidak)");
				ulang = input.next().toLowerCase();
				if (ulang.toLowerCase().equals("y")) {
					System.out.print("Masukkan kartu awal: ");
					kartu = input.nextInt();
				} else {
					System.out.println("Terimakasih sudah bermain, see you later..");
					break;
				}
			} else {
				System.out.print("Lanjutkan? (Y untuk ya, N untuk tidak) ");
				ulang = input.next().toLowerCase();
				if (ulang.toLowerCase().equals("n")) {
					System.out.print("Terimakasih sudah bermain, see you later..");
				}
			}

		}

	}

	private static void soal8() {

		System.out.println("*Soal8 - Deret Angka*");

		System.out.print("Masukkan panjang deret: ");
		int n = input.nextInt();

		int[][] larik = new int[2][n];

		int x = 0;
		for (int i = 0; i < (n); i++) {
			for (int j = 0; j < 2; j++) {

				larik[j][i] = x;
				x++;

			}
		}

		for (int j = 0; j < n; j++) {

			if (j == n - 1) {
				System.out.print((larik[0][j] + larik[1][j]));
			} else {
				System.out.print((larik[0][j] + larik[1][j]) + ", ");
			}

		}
		System.out.println();

	}

	private static void soal9() {

		System.out.println("*Soal9 - Menghitung Gunung dan Lembah*");

		System.out.println("Masukkan sinyal yang akan dicek: ");
		String inputan = input.next().toUpperCase();

		char[] sinyal = inputan.toCharArray();
		int gunung = 0;
		int lembah = 0;
		int mdpl = 0;
		int[] mdplArr = new int[sinyal.length];

		for (int i = 0; i < sinyal.length; i++) {

			if (sinyal[i] != 'T' && sinyal[i] != 'N') {
				System.out.println("Sinyal salah");
				return;
			} else if (sinyal[i] == 'N') {
				mdpl++;
				mdplArr[i] = mdpl;
			} else if (sinyal[i] == 'T') {
				mdpl--;
				mdplArr[i] = mdpl;
			}

			if (i > 0 && mdplArr[i] == 0) {

				if (mdplArr[i - 1] == 1) {
					gunung++;
				} else if (mdplArr[i - 1] == -1) {
					lembah++;
				}

			}

		}

		System.out.println("Jumlah gunung: " + gunung);
		System.out.println("Jumlah lembah: " + lembah);

	}

	private static void soal10() {

		System.out.println("*Soal10 - Perhitungan diskon maksimal*");

		System.out.print("Silahkan masukkan Saldo OPO (Idr): ");
		int saldoAwal = input.nextInt();

		int hargaKopi = 18000;

//		maks pembelian setelah mendapat diskon 50%

		if (saldoAwal >= 40000 && saldoAwal <= 100000) {
			saldoAwal *= 2;
		} else if (saldoAwal > 100000) {
			saldoAwal += 100000;
		}

//		jumlah kopi yang bisa dibeli
		int jumlahKopi = saldoAwal / hargaKopi;

		int cashback = (jumlahKopi * hargaKopi) / 20;
		int sisaSaldo = (saldoAwal % hargaKopi)/2;

		int saldoAkhir = cashback + sisaSaldo;

		System.out.println("Jumlah cup: " + jumlahKopi);
		System.out.println("Saldo Akhir : " + saldoAkhir);

	}

}
