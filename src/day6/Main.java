package day6;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {

		System.out.println("*Soal 1 - Fibonaci*");

		System.out.println("Masukkan Nilai N: ");
		int inputF = input.nextInt();

		int[] fibonaci = new int[inputF];
		String hasil = "";

		for (int i = 0; i < inputF; i++) {

			if (i > 1) {
				fibonaci[i] = fibonaci[i - 1] + fibonaci[i - 2];
			} else {
				fibonaci[i] = 1;
			}

			if (i == inputF - 1) {
				hasil = hasil + fibonaci[i];
			} else {
				hasil = hasil + fibonaci[i] + ",";
			}

		}
		System.out.println(hasil);

	}

	private static void soal2() {

		System.out.println("*Soal 2 - Fibonaci ke-2*");

		System.out.println("Masukkan Nilai N: ");
		int inputF2 = input.nextInt();

		int[] fibonaci2 = new int[inputF2];
		String hasil = "";

		for (int i = 0; i < inputF2; i++) {

			if (i > 2) {
				fibonaci2[i] = fibonaci2[i - 1] + fibonaci2[i - 2] + fibonaci2[i - 3];
			} else {
				fibonaci2[i] = 1;
			}

			if (i == inputF2 - 1) {
				hasil = hasil + fibonaci2[i];
			} else {
				hasil = hasil + fibonaci2[i] + ",";
			}

		}
		System.out.println(hasil);

	}

	private static void soal3() {

		System.out.println("*Soal 3 - Bilangan Prima*");

		System.out.println("Masukkan Nilai N: ");
		int inputP = input.nextInt();

		String hasil = "";

		for (int i = 2; i < inputP; i++) {
			int status = 0;
			for (int j = 2; j < i; j++) {

				if (i % j == 0) {
					status++;
				}

			}

			if (status == 0) {
				hasil += i + ",";
			}

		}
		System.out.println(hasil.substring(0, hasil.length() - 1));

	}

	private static void soal4() {

		System.out.println("*Soal 4 - Time Conversion*");

		System.out.print("Masukkan Waktu yang akan dikonversi (HH:MM:SS[AM/PM]): ");
		String inputWaktu = input.next();

		if (inputWaktu.length() != 10) {
			System.out.println("Maaf, waktu yang anda masukkan salah karena tidak memenuhi kaidah AM/PM");
			return;
		}

		String waktu = inputWaktu.substring(0, 8);
		String[] waktuStr = waktu.split(":");
		int[] waktuInt = new int[waktuStr.length];
		String hasil = "";
		String ampm = inputWaktu.substring(8, 10).toLowerCase();

		for (int i = 0; i < waktuStr.length; i++) {
			waktuInt[i] = Integer.parseInt(waktuStr[i]);
		}

		if (waktuInt[0] > 0 && waktuInt[0] <= 12) {

			if (ampm.equals("pm")) {
				if (waktuInt[0] < 12 && waktuInt[0] > 0) {
					waktuInt[0] += 12;
					waktuStr[0] = "" + waktuInt[0];
				}
			} else if (ampm.equals("am")) {
				if (waktuInt[0] == 12) {
					waktuStr[0] = "00";
				}
			}
			hasil = waktuStr[0] + ":" + waktuStr[1] + ":" + waktuStr[2];

		} else {
			System.out.println("Maaf, waktu yang anda masukkan salah karena tidak memenuhi kaidah AM/PM");
		}

		System.out.println(hasil);
	}

	private static void soal5() {

		System.out.println("*Soal 5 - Pohon Faktor*");

		System.out.print("Silahkan Masukkan angka yang akan dibuat pohon faktor: ");
		int masukan = input.nextInt();
		int proses1 = masukan;

		for (int i = 2; i <= masukan; i++) {

			while (proses1 % i == 0) {
				int proses2 = proses1 / i;
				System.out.println(proses1 + "/" + i + "=" + proses2);
				proses1 = proses2;
			}

		}

	}

	private static void soal6() {

		System.out.println("         *Soal 6 - Perhitungan Bensin*           ");
		System.out.println("_________________________________________________");
		System.out.println("jalur 1. Jarak dari Toko ke Customer 1       = 2.0 KM");
		System.out.println("jalur 2. Jarak dari Customer 1 ke Customer 2 = 0.5 KM");
		System.out.println("jalur 3. Jarak dari Customer 2 ke Customer 3 = 1.5 KM");
		System.out.println("jalur 4. Jarak dari Customer 3 ke Customer 4 = 0.3 KM");
		System.out.println("Masukkan jalur yang ditempuh oleh driver (pisahkan dengan spasi)");
		String masukan = input.nextLine();

		String[] masukanStr = masukan.split(" ");
		int[] masukanInt = new int[masukanStr.length];
		double jarakTempuh = 0;
		double rasioBensin = 2.5;
		double[] jarak = { 2.0, 0.5, 1.5, 0.3 };

//		double[][] jarak2 = {
//				{ 0.0, 2.0, 2.5, 4.0, 4.3 },
//				{ 2.0, 0.0, 0.5, 2.0, 2.3 },
//				{ 2.5, 0.5, 0.0, 1.5, 1.8 },
//				{ 4.0, 2.0, 1.5, 0.0, 0.3 },
//				{ 4.3, 2.3, 1.8, 0.3, 0.0 }
//		};

		String hasil = "Jarak Tempuh = ";

		for (int i = 0; i < masukanStr.length; i++) {
			masukanInt[i] = Integer.parseInt(masukanStr[i]);
			if (masukanInt[i] > 4) {
				System.out.println("maaf, jalur yang anda masukkan tidak tersedia.");
				return;
			}
		}

		for (int j = 0; j < masukanInt.length; j++) {
			jarakTempuh += jarak[masukanInt[j] - 1];
			hasil = hasil + jarak[masukanInt[j] - 1] + " KM + ";
		}

		double konsumsiBensin = Math.ceil(jarakTempuh / rasioBensin);
		hasil = hasil.substring(0, hasil.length() - 2);

		System.out.println(hasil + "= " + jarakTempuh + " KM");
		System.out.println("Bensin = " + (int) konsumsiBensin + " Liter");

	}

	private static void soal7() {

		System.out.println("*Soal 7 - Sinyal SOS*");
		System.out.println("Masukkan sinyal SOS yang akan dikirimkan: ");
		String sinyalMasukan = input.next();
		sinyalMasukan = sinyalMasukan.toLowerCase();
		char sinyalChar[] = sinyalMasukan.toCharArray();
		int salah = 0;
		for (int i = 0; i < sinyalMasukan.length(); i += 3) {

			if (sinyalChar[i] != 's') {
				salah++;
			}
			if (sinyalChar[i + 1] != 'o') {
				salah++;
			}
			if (sinyalChar[i + 2] != 's') {
				salah++;
			}

		}
		System.out.println("Sehingga total sinyal yang salah adalah: " + salah);

	}

}
