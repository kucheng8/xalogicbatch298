package day3;

import java.util.Scanner;

public class WhileLoop {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("masukkan nilai perhitungan: ");
		int n = input.nextInt();

		int i = 0;

		while (i < n) {
			System.out.println(i + 1);
			i++;
		}

		System.out.println();
		i = n;
		while (i > 0) {
			System.out.println(i);
			i--;
		}

		input.close();

	}

}
