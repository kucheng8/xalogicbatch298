package day3;

import java.util.Scanner;

public class NestedLoopFor {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {

				if (i + j >= n - 1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();

		}

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {

				if (i < j) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();

		}

//_________________________________________________________________________

		System.out.println();
		
		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {

				if (i==(n-1)/2 || j==(n-1)/2) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();

		}

//_________________________________________________________________________

		System.out.println();

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {

				if (i == j || i + j == n - 1) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();

		}
		
//______________________________________________________		
		System.out.println();

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {

				if (i >= j && i + j <= n - 1 ) {
					System.out.print("* ");
				} else {
					System.out.print("- ");
				}

			}
			System.out.println();

		}

		input.close();

	}

}
