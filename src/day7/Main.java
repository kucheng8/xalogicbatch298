package day7;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal3() {

		System.out.println("*Soal 3 - Simple Array Sum*");

		System.out.print("Masukkan Jumlah Anggota: ");
		int panjang = input.nextInt();

//		int[] larik = new int[panjang];
		int anggota = 0;
		int hitung = 0;
		String hasil = "";

		for (int i = 0; i < panjang; i++) {
			System.out.print("Masukkan nilai elemen anggota ke-" + (i + 1) + ": ");
			anggota = input.nextInt();
			hitung += anggota;
			hasil += anggota + "+";
		}

		System.out.println(hasil.substring(0, hasil.length() - 1) + "=" + hitung);

	}

	private static void soal4() {

		System.out.println("*Soal 4 - Diagonal Difference*");

		System.out.print("Masukkan Ukuran Array: ");
		int ukuran = input.nextInt();

		int[][] matriks = new int[ukuran][ukuran];

		for (int baris = 0; baris < ukuran; baris++) {
			for (int kolom = 0; kolom < ukuran; kolom++) {

				System.out.print("Masukkan nilai elemen matriks index [" + baris + "][" + kolom + "]: ");
				matriks[baris][kolom] = input.nextInt();
			}
		}

		int diagonal1 = 0;
		for (int i = 0; i < ukuran; i++) {
			diagonal1 += matriks[i][i];
		}

		int diagonal2 = 0;
		for (int j = 0; j < ukuran; j++) {
			diagonal2 += matriks[j][(ukuran - 1) - j];
		}

		int difference = Math.abs(diagonal1 - diagonal2);
		System.out.println("Diagonal 1= " + diagonal1);
		System.out.println("Diagonal 2= " + diagonal2);
		System.out.println("Difference = " + difference);
	}

	private static void soal5() {

		System.out.println("*Soal 5 - Plus Minus*");
		System.out.print("Masukkan Jumlah Anggota: ");
		int panjang = input.nextInt();

//		int[] larik = new int[panjang];

		int anggota = 0;
		double pos = 0;
		double nol = 0;
		double neg = 0;

		for (int i = 0; i < panjang; i++) {
			System.out.print("Masukkan nilai elemen anggota ke-" + (i + 1) + ": ");
			anggota = input.nextInt();

			if (anggota < 0) {
				neg++;
			} else if (anggota == 0) {
				nol++;
			} else if (anggota > 0) {
				pos++;
			}

		}

		DecimalFormat dec = new DecimalFormat("0.000000");

		System.out.println("Proporsi nilai positif = " + dec.format(pos / panjang));
		System.out.println("Proporsi nilai negatif = " + dec.format(neg / panjang));
		System.out.println("Proporsi nilai nol     = " + dec.format(nol / panjang));

	}

	private static void soal6() {

		System.out.println("*Soal 6 - Staircase*");

		System.out.println("Masukkan Tinggi Tangga: ");
		int n = input.nextInt();

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {

				if (i + j >= n - 1) {
					System.out.print("#");
				} else {
					System.out.print(" ");
				}

			}
			System.out.println();

		}

	}

	private static void soal7() {

		System.out.println("*Soal 7 Mini-Max Sum*");

		int[] larik = new int[5];
		int total = 0;

		for (int i = 0; i < 5; i++) {
			System.out.print("Masukkan nilai elemen array ke-" + (i + 1) + ": ");
			larik[i] = input.nextInt();
			total += larik[i];
		}
		int min = total;
		int max = 0;
		int proses = 0;

		for (int j = 0; j < 5; j++) {
			proses = total - larik[j];
			if (proses < min) {
				min = proses;
			}
			if (proses > max) {
				max = proses;
			}
		}
		System.out.println("Nilai Maksimal = " + max);
		System.out.println("Nilai Minimal  = " + min);

	}

	private static void soal8() {

		System.out.println("*Soal 8 - Birthday Cake Candle*");

		System.out.print("Masukkan Jumlah Lilin: ");
		int jumlah = input.nextInt();

		int[] larik = new int[jumlah];
		int max = 0;
		int hasil = 0;

		for (int i = 0; i < jumlah; i++) {
			System.out.print("Masukkan tinggi lilin ke-" + (i + 1) + ": ");
			larik[i] = input.nextInt();
			if (larik[i] > max) {
				max = larik[i];
			}
		}
		
		for (int j = 0; j <jumlah; j++) {
			if (larik[j]==max) {
				hasil++;
			}
		}
		
		System.out.println("Sehingga, jumlah lilin yang ditiup adalah: " + hasil);

	}

	private static void soal9() {

		System.out.println("*Soal 9 - A Very Big Sum*");

		System.out.print("Masukkan Jumlah Anggota Penjumlahan : ");
		int jumlah = input.nextInt();

//		long[] larik = new long[jumlah];

		long anggota = 0;

		long total = 0;

		for (int i = 0; i < jumlah; i++) {
			System.out.print("Masukkan Anggota ke-" + (i + 1) + ": ");
			anggota = input.nextLong();
			total += anggota;
		}

		System.out.println("Sehingga, jumlah penjumlahan dari anggota diatas adalah " + total);

	}

	private static void soal10() {

		System.out.println("*Soal 10 - Compare the Triplets*");

		int[][] matriks = new int[2][3];
//		int[] hasil = new int[2];
		int poinA = 0;
		int poinB = 0;

		for (int baris = 0; baris < 2; baris++) {
			for (int kolom = 0; kolom < 3; kolom++) {

				System.out.print("Masukkan nilai elemen matriks index [" + baris + "][" + kolom + "]: ");
				matriks[baris][kolom] = input.nextInt();
			}
		}

		for (int i = 0; i < 3; i++) {
			if (matriks[0][i] > matriks[1][i]) {
				poinA++;
			}
			if (matriks[1][i] > matriks[0][i]) {
				poinB++;
			}
		}
//		hasil[0] = poinA;
//		hasil[1] = poinB;

		System.out.println(poinA + " " + poinB);

	}

}
