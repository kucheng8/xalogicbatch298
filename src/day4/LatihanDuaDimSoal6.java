package day4;

import java.util.Scanner;

public class LatihanDuaDimSoal6 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo6 = new int[3][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			if (baris % 3 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo6[baris][kolom] = i;
					System.out.print(soalDuaDimNo6[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 3 == 1) {

				i = 1;
				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo6[baris][kolom] = i;
					System.out.print(soalDuaDimNo6[baris][kolom] + " ");
					i = i * m;
				}

				System.out.println();
			} else if (baris % 3 == 2) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo6[baris][kolom] = soalDuaDimNo6[baris-2][kolom] + soalDuaDimNo6[baris-1][kolom];
					System.out.print(soalDuaDimNo6[baris][kolom] + " ");
					i++;

				}
			}
		}

		input.close();

	}

}
