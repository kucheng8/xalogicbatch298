package day4;

import java.util.Scanner;

public class ArrayTwoDimension {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();
		System.out.println("Masukkan nilai M: ");
		int m = input.nextInt();

		int i = 0;

		int[][] arrayAngkaDuaDim = new int[n][m];

		System.out.println();
		System.out.println("Matriks " + n + " X " + m);
		System.out.println();

		for (int baris = 0; baris < n; baris++) {

			for (int kolom = 0; kolom < m; kolom++) {

				i++;
				arrayAngkaDuaDim[baris][kolom] = i;
				System.out.print(arrayAngkaDuaDim[baris][kolom] + " ");

			}
			System.out.println();

		}

		input.close();

	}

}
