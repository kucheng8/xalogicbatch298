package day4;

import java.util.Scanner;

public class LatihanSatuDimSoal1 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 1;

		int[] soalSatu = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			soalSatu[baris] = i;
			System.out.print(soalSatu[baris] + " ");
			i = i + 2;

		}

		input.close();

	}

}
