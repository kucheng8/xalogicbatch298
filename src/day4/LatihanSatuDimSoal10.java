package day4;

import java.util.Scanner;

public class LatihanSatuDimSoal10 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 1;

		int[] soalSepuluh = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			if (baris % 4 == 3) {
				i = i * 3;
				System.out.print("XXX ");
			} else {
				i = i * 3;
				soalSepuluh[baris] = i;
				System.out.print(soalSepuluh[baris] + " ");
			}

		}

		input.close();

	}

}
