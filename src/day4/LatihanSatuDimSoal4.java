package day4;

import java.util.Scanner;

public class LatihanSatuDimSoal4 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 1;

		int[] soalEmpat = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			soalEmpat[baris] = i;
			System.out.print(soalEmpat[baris] + " ");
			i = i + 4;

		}

		input.close();

	}

}
