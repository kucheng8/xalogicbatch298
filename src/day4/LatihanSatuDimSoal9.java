package day4;

import java.util.Scanner;

public class LatihanSatuDimSoal9 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 1;

		int[] soalSembilan = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			if (baris % 3 == 2) {
				System.out.print("* ");
			} else {
				i = i * 4;
				soalSembilan[baris] = i;
				System.out.print(soalSembilan[baris] + " ");
			}

		}

		input.close();

	}

}
