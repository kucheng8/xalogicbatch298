package day4;

import java.util.Scanner;

public class LatihanDuaDimSoal8 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo8 = new int[3][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			if (baris % 3 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo8[baris][kolom] = i;
					System.out.print(soalDuaDimNo8[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 3 == 1) {
				i = 0;

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo8[baris][kolom] = i;
					System.out.print(soalDuaDimNo8[baris][kolom] + " ");
					i = i + 2;

				}

				System.out.println();
			} else if (baris % 3 == 2) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo8[baris][kolom] = soalDuaDimNo8[0][kolom] + soalDuaDimNo8[1][kolom];
					System.out.print(soalDuaDimNo8[baris][kolom] + " ");
					i++;

				}
			}
		}

		input.close();

	}

}
