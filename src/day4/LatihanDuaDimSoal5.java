package day4;

import java.util.Scanner;

public class LatihanDuaDimSoal5 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo5 = new int[3][n];

		System.out.println();
		System.out.println("Matriks 3 X " + n);
		System.out.println();

		for (int baris = 0; baris < 3; baris++) {

			for (int kolom = 0; kolom < n; kolom++) {

				soalDuaDimNo5[baris][kolom] = i;
				System.out.print(soalDuaDimNo5[baris][kolom] + " ");
				i++;

			}
			System.out.println();

		}

		input.close();

	}

}
