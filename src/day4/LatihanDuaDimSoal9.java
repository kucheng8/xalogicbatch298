package day4;

import java.util.Scanner;

public class LatihanDuaDimSoal9 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();
		System.out.println("Masukkan nilai N2: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo9 = new int[n][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < n; baris++) {

			if (baris % 3 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo9[baris][kolom] = i;
					System.out.print(soalDuaDimNo9[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 3 == 1) {
				i = 0;

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo9[baris][kolom] = i;
					System.out.print(soalDuaDimNo9[baris][kolom] + " ");
					i = i + n;

				}
				System.out.println();
			} else if (baris % 3 == 2) {
				i = i - n;

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo9[baris][kolom] = i;
					System.out.print(soalDuaDimNo9[baris][kolom] + " ");
					i = i - n;

				}
				System.out.println();
			}
		}

		input.close();

	}

}
