package day4;

import java.util.Scanner;

public class LatihanSatuDimSoal3 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 1;

		int[] soalTiga = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			soalTiga[baris] = i;
			System.out.print(soalTiga[baris] + " ");
			i = i + 3;

		}

		input.close();

	}

}
