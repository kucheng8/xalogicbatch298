package day4;

import java.util.Scanner;

public class LatihanDuaDimSoal3 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();
		System.out.println("Masukkan nilai N2: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo3 = new int[n][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 2; baris++) {

			if (baris % 2 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo3[baris][kolom] = i;
					System.out.print(soalDuaDimNo3[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 2 == 1) {

				i = n;
				for (int kolom = 0; kolom < m; kolom++) {

					if (kolom % 6 == 3 ||kolom % 6 == 4 || kolom % 6 == 5) {

						soalDuaDimNo3[baris][kolom] = i;
						System.out.print(soalDuaDimNo3[baris][kolom] + " ");
						i = i / 2;

					} else {

						soalDuaDimNo3[baris][kolom] = i;
						System.out.print(soalDuaDimNo3[baris][kolom] + " ");
						i = i * 2;

					}

				}

				System.out.println();

			}
		}

		input.close();

	}

}
