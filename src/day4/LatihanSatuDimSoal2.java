package day4;

import java.util.Scanner;

public final class LatihanSatuDimSoal2 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int n = input.nextInt();

		int i = 0;

		int[] soalDua = new int[n];

		System.out.println();
		System.out.println("Matriks 1 X " + n);

		for (int baris = 0; baris < n; baris++) {

			i = i + 2;
			soalDua[baris] = i;
			System.out.print(soalDua[baris] + " ");

		}

		input.close();

	}

}
