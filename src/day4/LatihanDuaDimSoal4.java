package day4;

import java.util.Scanner;

public class LatihanDuaDimSoal4 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai N: ");
		int m = input.nextInt();
		System.out.println("Masukkan nilai N2: ");
		int n = input.nextInt();

		int i = 0;

		int[][] soalDuaDimNo4 = new int[n][m];

		System.out.println();
		System.out.println("Matriks hasilnya");
		System.out.println();

		for (int baris = 0; baris < 2; baris++) {

			if (baris % 2 == 0) {

				for (int kolom = 0; kolom < m; kolom++) {

					soalDuaDimNo4[baris][kolom] = i;
					System.out.print(soalDuaDimNo4[baris][kolom] + " ");
					i++;

				}
				System.out.println();
			} else if (baris % 2 == 1) {

				i = 1;
				int j = 5;
				for (int kolom = 0; kolom < m; kolom++) {

					if (kolom % 2 == 0) {
						soalDuaDimNo4[baris][kolom] = i;
						System.out.print(soalDuaDimNo4[baris][kolom] + " ");
						i++;
					} else {
						soalDuaDimNo4[baris][kolom] = j;
						System.out.print(soalDuaDimNo4[baris][kolom] + " ");
						j = j + 5;
					}

				}

				System.out.println();

			}
		}

		input.close();

	}

}
