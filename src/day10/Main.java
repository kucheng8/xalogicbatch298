package day10;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

//		String answer = "Y";
//
//		while (answer.toUpperCase().equals("Y")) {
//
//			try {
//
//				System.out.println("Enter the number of the case: ");
//				int number = input.nextInt();
//				input.nextLine();
//
//				switch (number) {
//				case 1:
//		soal1();
//					break;
//				case 2:
//		soal2();
//					break;
//				case 3:
//		soal3();
//					break;
//				case 4:
		soal4();
//					break;
//				case 5:
//					soal5();
//					break;
//				case 6:
//		soal6();
//					break;
//				case 7:
//		soal7();
//					break;
//				case 8:
//		soal8();
//					break;
//				default:
//					System.out.println("Case is not available");
//					break;
//
//				}
//
//			} catch (Exception e) {
//				System.out.println(e.getMessage());
//			}
//
//			System.out.println();
//			System.out.println("Continue?");
//			answer = input.next();
//		}

	}

	private static void soal1() {

		System.out.println("*Soal 1 - Big Sorting*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();

		BigDecimal[] larik = new BigDecimal[jumlah];

		for (int i = 0; i < jumlah; i++) {
			System.out.print("Masukkan data ke-" + (i + 1) + ": ");
			larik[i] = input.nextBigDecimal();
		}

		for (int i = 0; i < jumlah; i++) {
			for (int j = 0; j < jumlah; j++) {

				if (larik[i].compareTo(larik[j]) == -1) {
					BigDecimal temp = larik[i];
					larik[i] = larik[j];
					larik[j] = temp;
				}

			}
		}
		for (int i = 0; i < jumlah; i++) {
			System.out.println(larik[i]);
		}

	}

	private static void soal2() {

		System.out.println("*Soal 2 - Insertion Sort*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan data: ");
		String data = input.nextLine();

		String[] larik = data.split(" ");

		if (larik.length != jumlah) {
			System.out.println("Maaf, data yang anda masukkan tidak sinkron (tidak sama jumlah).");
			return;
		}

//		int[] larikOriginal = new int[jumlah];
		int[] larikUrut = new int[jumlah];
//		int[] larikSimpan = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
//			larikOriginal[i] = Integer.parseInt(larik[i]);
			larikUrut[i] = Integer.parseInt(larik[i]);
//			larikSimpan[i] = Integer.parseInt(larik[i]);
		}

		int temp = 0;
//		for (int i = 0; i < jumlah; i++) {
//			for (int j = 0; j < jumlah; j++) {
//
//				if (larikUrut[i] <= larikUrut[j]) {
//					temp = larikUrut[i];
//					larikUrut[i] = larikUrut[j];
//					larikUrut[j] = temp;
//				}
//			}
//			for (int k = 0; k < jumlah; k++) {
//				System.out.print(" " + larikUrut[k]);
//			}
//			System.out.println();
//		}
		for (int i = jumlah - 1; i > 0; i--) {
			for (int j = jumlah - 1; j >= 0; j--) {
				if (larikUrut[i] >= larikUrut[j]) {
					temp = larikUrut[i];
					larikUrut[i] = larikUrut[j];
					larikUrut[j] = temp;
					break;
				}
			}
			for (int k = 0; k < jumlah; k++) {

				if (larikUrut[k] == 3 && i > 1) {
					if (k == 0) {
						System.out.print(larikUrut[k - 1]);
					} else {
						System.out.print(" " + larikUrut[k - 1]);
					}
				} else {
					if (k == 0) {
						System.out.print(larikUrut[k]);
					} else {
						System.out.print(" " + larikUrut[k]);
					}
				}

			}
			System.out.println();
		}

//		for (int k = 0; k < jumlah; k++) {
//			if (k == 0) {
//
//				System.out.print(larikUrut[k]);
//			} else {
//				System.out.print(" " + larikUrut[k]);
//			}
//		}

	}

	private static void soal3() {

		System.out.println("*Soal 3 - Correctness and the Loop Invariant*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan data: ");
		String data = input.nextLine();

		String[] larik = data.split(" ");

		if (larik.length != jumlah) {
			System.out.println("Maaf, data yang anda masukkan tidak sinkron (tidak sama jumlah).");
			return;
		}

		int[] larikUrut = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			larikUrut[i] = Integer.parseInt(larik[i]);
		}

		int temp = 0;
		for (int i = 0; i < jumlah; i++) {
			for (int j = 0; j < jumlah; j++) {

				if (larikUrut[i] <= larikUrut[j]) {
					temp = larikUrut[i];
					larikUrut[i] = larikUrut[j];
					larikUrut[j] = temp;
				}
			}
		}

		for (int k = 0; k < jumlah; k++) {

			if (k == 0) {
				System.out.print(larikUrut[k]);
			} else {
				System.out.print(" " + larikUrut[k]);
			}
		}

	}

	private static void soal4() {

		System.out.println("*Soal 4 - Running Time of Algorithms*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan data: ");
		String data = input.nextLine();

		String[] larik = data.split(" ");

		if (larik.length != jumlah) {
			System.out.println("Maaf, data yang anda masukkan tidak sinkron (tidak sama jumlah).");
			return;
		}

		int[] larikUrut = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			larikUrut[i] = Integer.parseInt(larik[i]);
		}

		int step = 0;
		int temp = 0;
		for (int i = 0; i < jumlah; i++) {

			for (int j = i; j < jumlah; j++) {

				if (larikUrut[i] > larikUrut[j]) {
					temp = larikUrut[j];
					larikUrut[j] = larikUrut[i];
					larikUrut[i] = temp;
					step++;
				}
			}
		}
		System.out.print(step);

	}

	private static void soal5() {

		System.out.println("*Soal 5 - Counting Sort 1*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan data: ");
		String data = input.nextLine();

		String[] lrk = data.split(" ");

		if (lrk.length != jumlah) {
			System.out.println("Maaf, data yang anda masukkan tidak sinkron (tidak sama jumlah).");
			return;
		}

		int[] larik = new int[jumlah];
		int[] jml = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			larik[i] = Integer.parseInt(lrk[i]);
			jml[larik[i]] += 1;
		}

		for (int i = 0; i < jumlah; i++) {
			System.out.print(jml[i] + " ");
		}

	}

	private static void soal6() {

		System.out.println("*Soal 6 - Counting Sort 2*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan data: ");
		String data = input.nextLine();

		String[] lrk = data.split(" ");

		if (lrk.length != jumlah) {
			System.out.println("Maaf, data yang anda masukkan tidak sinkron (tidak sama jumlah).");
			return;
		}

		int[] larik = new int[jumlah];
		int[] jml = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			larik[i] = Integer.parseInt(lrk[i]);
			jml[larik[i]] += 1;
		}

		for (int i = 0; i < jumlah; i++) {
			if (jml[i] > 0) {
				for (int j = 0; j < jml[i]; j++) {
					System.out.print(i + " ");
				}
			}
		}

	}

	private static void soal7() {

		System.out.println("*Soal 7 - Find the Median*");

		System.out.print("Masukkan jumlah data yang akan diinput: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan data: ");
		String data = input.nextLine();

		String[] lrk = data.split(" ");

		if (lrk.length != jumlah) {
			System.out.println("Maaf, data yang anda masukkan tidak sinkron (tidak sama jumlah).");
			return;
		}

		int[] larik = new int[jumlah];

		for (int i = 0; i < jumlah; i++) {
			larik[i] = Integer.parseInt(lrk[i]);
		}

		int temp = 0;
		for (int i = 0; i < jumlah; i++) {
			for (int j = 0; j < jumlah; j++) {

				if (larik[i] < larik[j]) {
					temp = larik[i];
					larik[i] = larik[j];
					larik[j] = temp;
				}

			}
		}

		int median = 0;
		if (jumlah % 2 == 1) {
			median = larik[((jumlah + 1) / 2) - 1];
		} else {
			median = (larik[(jumlah / 2) - 1] + larik[(jumlah / 2)]) / 2;
		}

		System.out.println(median);

	}

	private static void soal8() {

		System.out.println("*Soal 8 - Sorting the Alphabets*");

		System.out.print("Masukkan Sampel: ");
		String data = input.nextLine();

		char[] larik = data.toCharArray();

		char temp = ' ';
		for (int i = 0; i < larik.length; i++) {
			for (int j = 0; j < larik.length; j++) {

				if (larik[i] < larik[j]) {
					temp = larik[i];
					larik[i] = larik[j];
					larik[j] = temp;
				}

			}
		}

		for (int i = 0; i < larik.length; i++) {
			System.out.print(larik[i]);
		}

	}

}
