package Day14;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() throws ParseException {

		System.out.println("*Soal1 - Libur Mary & Susan*");

		System.out.print("Masukkan jumlah hari kerja Merry: ");
		int x = input.nextInt();
		x++;
		System.out.print("Masukkan jumlah hari kerja Susan: ");
		int y = input.nextInt();
		y++;
		input.nextLine();
		System.out.print("Silahkan masukkan tanggal libur berbarengan (dd MMMM yyyy): "); // 25 februari 2020
		String tanggal = input.nextLine().toLowerCase();
		String[] tgl = tanggal.split(" ");

		int salah = 0;

		for (int i = 0; i < tanggal.length(); i++) {

			if (((int) tanggal.charAt(i) >= 48 && (int) tanggal.charAt(i) <= 57)
					|| ((int) tanggal.charAt(i) >= 97 && (int) tanggal.charAt(i) <= 122)
					|| ((int) tanggal.charAt(i) == 32)) {
				salah = 0;
			} else {
				salah++;
				System.out.println("Maaf, inputan anda salah.");
				return;
			}
		}

		String kosong = "";
		SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
		Date tanggalLibur = df.parse(tanggal);

		int tanggalInt = Integer.parseInt(tgl[0]);
		@SuppressWarnings("deprecation")
		int bulan = tanggalLibur.getMonth();
		System.out.println(bulan);
		int tahun = Integer.parseInt(tgl[2]);

		boolean tahunKabisat = false;

		if (tahun % 4 == 0) {
			if (tahun % 100 == 0) {
				if (tahun % 400 == 0) {
					tahunKabisat = true;
				} else {
					tahunKabisat = false;
				}
			} else {
				tahunKabisat = true;
			}
		} else {
			tahunKabisat = false;
		}

		if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12) {
			if (tanggalInt < 0 || tanggalInt > 31) {
				salah++;
			}
		} else if (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11) {
			if (tanggalInt < 0 || tanggalInt > 30) {
				salah++;
			}
		} else if (tahunKabisat && bulan == 2) {
			if (tanggalInt < 0 || tanggalInt > 29) {
				salah++;
			}
		} else if (!tahunKabisat && bulan == 2) {
			if (tanggalInt < 0 || tanggalInt > 28) {
				salah++;
			}
		} else if (tahun > 2022) {
			salah++;
		}

		if (salah > 0) {
			System.out.println("Maaf, inputan anda salah.");
			return;
		}

//		Step mencari KPK 
		int kpk = 0;
		if (x < y) {
			kpk = x;
		} else {
			kpk = y;
		}

		int hitung = 0;
		boolean val = true;
		while (val) {
			hitung += kpk;
			if (hitung % x == 0 && hitung % y == 0) {
				val = false;
			} else {
				val = true;
			}
		}

		long gapLiburInMiliSecond = hitung * 24 * 60 * 60 * 1000;
		long tanggalLiburLong = tanggalLibur.getTime() + gapLiburInMiliSecond;
		Date result = new Date(tanggalLiburLong);
		kosong = kosong + (String) df.format(result);
		System.out.println(kosong);

	}

	private static void soal2() {

		System.out.println("*Soal2 - Urutan Abjad*");
		System.out.print("Silahkan masukkan kata yang akan diurutkan: "); 
		String kata = input.nextLine().toLowerCase().replaceAll(" ", "");
		char[] larik = kata.toCharArray();
		
		for (int i = 0; i < larik.length; i++) {

			if ((int) larik[i] < 97 || (int) larik[i]> 122) {
				System.out.println("Maaf, inputan anda salah.");
				return;
			}
		}

		char temp = 0;
		for (int i = 0; i < larik.length; i++) {
			for (int j = 0; j < larik.length; j++) {

				if (larik[i] < larik[j]) {

					temp = larik[i];
					larik[i] = larik[j];
					larik[j] = temp;

				}

			}
		}

		for (int i = 0; i < larik.length; i++) {
			if (i == 0) {
				System.out.print(larik[i]);
			} else if (larik[i] != larik[i - 1] && i != 0) {
				System.out.print(" - " + larik[i]);
			} else {
				System.out.print(larik[i]);
			}
		}

	}

}
