package tugas_oop;

public class Car {

	// Atribut

	String nama;
	double kecepatan = 0;
	double kecepatanMaks = 300;
	double akselerasi = 0;
	double persentaseBahanBakar = 100;

	// Definisi method

	void lurus(int lama_lurusan) {
		int durasi = 0;
		for (int i = 1; i <= lama_lurusan; i++) {
			if (persentaseBahanBakar >= 0.3) {
				persentaseBahanBakar -= 0.3;
				kecepatan += akselerasi;
				if (kecepatan >= kecepatanMaks) {
					kecepatan = kecepatanMaks;
				} else if (kecepatan <= 0) {
					kecepatan = 0;
				}
				durasi++;
			} else {
				habisBahanBakar();
				break;
			}
		}
		
		System.out.println(nama + " melaju dengan kecepatan akhir lurusan " + kecepatan + "km/jam");
		System.out.println("setelah melaju selama " + durasi + " menit");
		System.out.println("dengan persentase sisa bahan bakar " + persentaseBahanBakar + "%");
		System.out.println("______________________________________________________________________");
	}

	void belok(int lama_belokan) {
		int durasi = 0;
		for (int i = 1; i <= lama_belokan; i++) {
			if (persentaseBahanBakar >= 0.2) {
				persentaseBahanBakar -= 0.2;
				kecepatan += akselerasi * 0.2;
				if (kecepatan >= kecepatanMaks) {
					kecepatan = kecepatanMaks;
				} else if (kecepatan <= 0) {
					kecepatan = 0;
				}
				durasi++;
			} else {
				habisBahanBakar();
				break;
			}
		}
		
		System.out.println(nama + " melaju dengan kecepatan akhir belokan " + kecepatan + "km/jam");
		System.out.println("setelah berbelok selama " + durasi + " menit");
		System.out.println("dengan persentase sisa bahan bakar " + persentaseBahanBakar + "%");
		System.out.println("______________________________________________________________________");
	}

	void tanjakan(int lama_tanjakan) {
		int durasi = 0;
		for (int i = 1; i <= lama_tanjakan; i++) {
			if (persentaseBahanBakar >= 0.3) {
				persentaseBahanBakar -= 0.3;
				kecepatan += akselerasi * 0.6;
				if (kecepatan >= kecepatanMaks) {
					kecepatan = kecepatanMaks;
				} else if (kecepatan <= 0) {
					kecepatan = 0;
				}
				durasi++;
			} else {
				habisBahanBakar();
				break;
			}
		}
		
		System.out.println(nama + " melaju dengan kecepatan akhir setelah tanjakan " + kecepatan + "km/jam");
		System.out.println("setelah melaju di tanjakan selama " + durasi + " menit");
		System.out.println("dengan persentase sisa bahan bakar " + persentaseBahanBakar + "%");
		System.out.println("______________________________________________________________________");
	}

	void turunan(int lama_turunan) {
		int durasi = 0;
		for (int i = 1; i <= lama_turunan; i++) {
			if (persentaseBahanBakar >= 0.3) {
				persentaseBahanBakar -= 0.3;
				kecepatan += akselerasi * 1.2;
				if (kecepatan >= kecepatanMaks) {
					kecepatan = kecepatanMaks;
				} else if (kecepatan <= 0) {
					kecepatan = 0;
				}
				durasi++;
			} else {
				habisBahanBakar();
				break;
			}
		}
		System.out.println(nama + " melaju dengan kecepatan akhir setelah turunan " + kecepatan + "km/jam");
		System.out.println("setelah melaju di turunan selama " + durasi + " menit");
		System.out.println("dengan persentase sisa bahan bakar " + persentaseBahanBakar + "%");
		System.out.println("______________________________________________________________________");
	}

	// method habisBahanBakar untuk mengecek nilai bahanBakar

	boolean habisBahanBakar() {
		if (persentaseBahanBakar <= 0)
			return true;
		return false;
	}

}
