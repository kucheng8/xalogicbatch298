package tugas_oop;

public class Main {

	public static void main(String[] args) {

		// Membuat objek Car
		Car ford = new Car();

		// Mengisi atribut car
		ford.nama = "Ford";
		ford.akselerasi = 10;
		ford.persentaseBahanBakar = 100;
		ford.kecepatanMaks = 500;

		// Menjalankan Method
		ford.lurus(1000);
		ford.belok(1000);
		ford.tanjakan(1000);
		ford.turunan(1000);

		if (ford.habisBahanBakar()) {
			System.out.println("Bahan bakar habis, tidak dapat melanjutkan race");
		}

	}

}
