package day8;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}

	}

	private static void soal1() {

		System.out.println("*Soal 1 - camelCase*");

		System.out.print("Masukkan Kalimat yang akan dihitung Camel Case: ");
		String kalimat = input.next();

		int jumlah = 1;

		for (int i = 0; i < kalimat.length(); i++) {
			if (kalimat.charAt(i) == kalimat.toUpperCase().charAt(i)) {
				jumlah++;
			}
		}
		System.out.println(jumlah);

	}

	private static void soal2() {

		System.out.println("*Soal 2 - Strong Password*");

		System.out.print("Masukkan jumlah karakter Password: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan Password: ");
		String pw = input.next();
		char[] pass = pw.toCharArray();

		if (jumlah != pw.length()) {
			System.out.println("Maaf, jumlah karakter yang anda masukkan tidak sesuai");
			return;
		}

		if (pw.length() < 6) {
			System.out.println(6 - pw.length());
			return;
		}

		String num = "0123456789";
		char[] number = num.toCharArray();
		String lc = "abcdefghijklmnopqrstuvwxyz";
		char[] lowCase = lc.toCharArray();
		String uc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		char[] upCase = uc.toCharArray();
		String sc = "!@#$%^&*()-+";
		char[] spChar = sc.toCharArray();

		int hNum = 1;
		int hLc = 1;
		int hUc = 1;
		int hSc = 1;

		for (int i = 0; i < pass.length; i++) {
			for (int j = 0; j < number.length; j++) {
				if (pass[i] == number[j]) {
					hNum = 0;
					break;
				}
			}
			for (int k = 0; k < lowCase.length; k++) {
				if (pass[i] == lowCase[k]) {
					hLc = 0;
					break;
				}
			}
			for (int l = 0; l < upCase.length; l++) {
				if (pass[i] == upCase[l]) {
					hUc = 0;
					break;
				}
			}
			for (int m = 0; m < spChar.length; m++) {
				if (pass[i] == spChar[m]) {
					hSc = 0;
					break;
				}
			}
		}

		System.out.println(hNum + hLc + hUc + hSc);

	}

	private static void soal3() {

		System.out.println("*Soal 3 - Caesar Cipher*");

		System.out.print("Masukkan jumlah karakter yang akan diubah menggunakan metode CC: ");
		int jumlah = input.nextInt();
		input.nextLine();
		System.out.print("Masukkan kata yang akan diubah menggunakan metode CC: ");
		String kal = input.next();
		input.nextLine();
		if (jumlah != kal.length()) {
			System.out.println("Maaf, jumlah karakter yang anda masukkan tidak sinkron.");
			return;
		}
		System.out.print("Masukkan jumlah rotasi yang akan dilakukan: ");
		int rotasi = input.nextInt();

		char[] kalimat = kal.toCharArray();

		String alf = "abcdefghijklmnopqrstuvwxyz";
		char[] alfabet = alf.toCharArray();
		char[] alfarot = alf.toCharArray();

		String alf2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		char[] alfabet2 = alf2.toCharArray();
		char[] alfarot2 = alf2.toCharArray();

		String hasil = "";

		char temp = 0;
		char temp2 = 0;
		for (int i = 0; i < rotasi; i++) {
			for (int j = 0; j < 26; j++) {
				if (j == 0) {
					temp = alfarot[0];
					temp2 = alfarot2[0];
					alfarot[j] = alfarot[j + 1];
					alfarot2[j] = alfarot2[j + 1];
				} else if (j == 25) {
					alfarot[j] = temp;
					alfarot2[j] = temp2;
				} else {
					alfarot[j] = alfarot[j + 1];
					alfarot2[j] = alfarot2[j + 1];
				}
			}
		}

		for (int k = 0; k < kalimat.length; k++) {
			boolean val = false;
			for (int l = 0; l < 26; l++) {
				if (kalimat[k] == alfabet[l]) {
					hasil += alfarot[l];
					val = true;
				} else if (kalimat[k] == alfabet2[l]) {
					hasil += alfarot2[l];
					val = true;
				}
			}
			if (val == false) {
				hasil += kalimat[k];
			}
		}
		System.out.println(hasil);

	}

	private static void soal4() {

		System.out.println("*Soal 4 - Mars Exploration*");
		System.out.println("Masukkan sinyal SOS yang akan dikirimkan: ");
		String sinyalMasukan = input.next();
		sinyalMasukan = sinyalMasukan.toLowerCase();
		char sinyalChar[] = sinyalMasukan.toCharArray();
		int salah = 0;
		for (int i = 0; i < sinyalMasukan.length(); i += 3) {

			if (sinyalChar[i] != 's') {
				salah++;
			}
			if (sinyalChar[i + 1] != 'o') {
				salah++;
			}
			if (sinyalChar[i + 2] != 's') {
				salah++;
			}

		}
		System.out.println("Sehingga total sinyal yang salah adalah: " + salah);

	}

	private static void soal5() {

		System.out.println("*Soal 5 - HackerRank in a String!*");

		System.out.print("Masukkan Jumlah Kata: ");
		int jumlah = input.nextInt();

		String[] kata = new String[jumlah];
		String[] hasil = new String[jumlah];

		String acuan = "hackerrank";
		for (int i = 0; i < jumlah; i++) {
			System.out.print("Masukkan kata ke-" + (i + 1) + ": ");
			kata[i] = input.next().toLowerCase();
		}

		for (int j = 0; j < jumlah; j++) {
			int l = 0;
			hasil[j] = "";
			for (int k = 0; k < kata[j].length(); k++) {
				while (l < acuan.length() && kata[j].charAt(k) == acuan.charAt(l)) {
					hasil[j] += acuan.charAt(l);
					l++;
				}
			}

			if (hasil[j].equals(acuan)) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}

	}

	private static void soal6() {

		System.out.println("*Soal 6 - Pangrams*");

		System.out.print("Masukkan Kalimat yang akan dicek pangrams: ");
		String kalimat = input.nextLine().toLowerCase();
		String alfabet = "abcdefghijklmnopqrstuvwxyz";
		String hasil = "";

		for (int i = 0; i < alfabet.length(); i++) {
			for (int j = 0; j < kalimat.length(); j++) {
				if (kalimat.charAt(j) == alfabet.charAt(i)) {
					hasil += alfabet.charAt(i);
					break;
				}
			}
		}

		if (hasil.equals(alfabet)) {
			System.out.println("pangram");
		} else {
			System.out.println("not pangram");
		}

	}

	private static void soal7() {

		System.out.println("*Soal 7 - Separate the Numbers*");
		System.out.println("Silahkan masukkan jumlah deret: ");
		int jumlah = input.nextInt();
		input.nextLine();

		String[] deret = new String[jumlah];

		for (int i = 0; i < jumlah; i++) {
			System.out.println("Masukkan deret ke-" + (i + 1));
			deret[i] = input.next();
		}

		for (int i = 0; i < jumlah; i++) {
			int first = 0;
			boolean biutipul = false;

			for (int j = 1; j <= (deret[i].length() / 2); j++) {

				int num = Integer.parseInt(deret[i].substring(0, j));
				first = num;

				String temp = "" + first;
				while (temp.length() < deret[i].length()) {
					num++;
					temp += num;

				}

				if (deret[i].equals(temp)) {
					biutipul = true;
					break;
				}

			}
			if (biutipul) {
				System.out.println("YES " + first);
			} else {
				System.out.println("NO");
			}
			System.out.println();
		}

	}

	private static void soal8() {

		System.out.println("*Soal 8 - gemstone*");

		System.out.println("Silahkan Masukkan jumlah sampel yang akan diperiksa: ");
		int jumlah = input.nextInt();

		String[] sampel = new String[jumlah];

		for (int i = 0; i < jumlah; i++) {
			System.out.println("Silahkan Masukkan sampel ke-" + (i + 1) + ": ");
			sampel[i] = input.next().toLowerCase();
		}

		String abjad = "abcdefghijklmnopqrstuvwxyz";
		int[] jumlahPerAbjad = new int[26];
		int hasil = 0;

		for (int i = 0; i < jumlah; i++) {

			for (int j = 0; j < abjad.length(); j++) {
				for (int k = 0; k < sampel[i].length(); k++) {
					if (sampel[i].charAt(k) == abjad.charAt(j)) {
						jumlahPerAbjad[j]++;
						break;
					}
				}
			}

		}
		
		for (int i = 0; i < 26; i++) {
			if (jumlahPerAbjad[i] == jumlah) {
				hasil++;
			}
		}
		System.out.println(hasil);

	}

	private static void soal9() {

		System.out.println("*Soal 9 - Making Anagrams*");
		String[] kata = new String[2];
		String hasil = "";
		int output = 0;
		for (int i = 0; i < 2; i++) {
			System.out.print("Masukkan kata ke-" + (i + 1) + ": ");
			kata[i] = input.next().toLowerCase();
		}

		for (int i = 0; i < kata[0].length(); i++) {
			for (int j = 0; j < kata[1].length(); j++) {
				if (kata[0].charAt(i) == kata[1].charAt(j)) {
					hasil += kata[0].charAt(i);
					break;
				}
			}
		}
		output = kata[0].length() + kata[1].length() - (hasil.length() * 2);

		System.out.println(hasil);
		System.out.println(output);

	}

	private static void soal10() {

		System.out.println("*Soal 10 - Two String*");
		System.out.print("Silahkan masukkan jumlah pasangan: ");
		int jumlah = input.nextInt();
		input.nextLine();
		String[][] pasangan = new String[jumlah][2];

		for (int i = 0; i < jumlah; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.print("Silahkan masukkan anggota pasangan ke-" + (i + 1) + " kata ke-" + (j + 1) + ": ");
				pasangan[i][j] = input.next().toLowerCase();
			}
		}

		for (int i = 0; i < jumlah; i++) {

			int p = 0;
			for (int j = 0; j < pasangan[i][0].length(); j++) {
				for (int k = 0; k < pasangan[i][1].length(); k++) {
					if (pasangan[0][0].charAt(j) == pasangan[0][1].charAt(k)) {
						p++;
						break;
					}
				}
			}
			if (p > 0) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}

		}

	}

}
