package day5;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {

		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {

			try {

				System.out.println("Enter the number of the case: ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not available");
					break;

				}

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Disini Soal 1");

		System.out.println("Masukkan jumlah uang Andi: "); // Input uang andi
		int uangAndi = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan Harga Baju (pisahkan menggunakan tanda koma  (,) tanpa spasi ) : "); // Input harga
		String hargaBaju = input.next();
		System.out.println("Masukkan Harga Celana (pisahkan menggunakan tanda koma  (,) tanpa spasi ) : "); // Input
		String hargaCelana = input.next();

		String[] bajuStr = hargaBaju.split(",");
		int[] bajuInt = new int[bajuStr.length];

		for (int i = 0; i < bajuStr.length; i++) {
			bajuInt[i] = Integer.parseInt(bajuStr[i]);
		}

		String[] celanaStr = hargaCelana.split(",");
		int[] celanaInt = new int[celanaStr.length];

		for (int i = 0; i < celanaStr.length; i++) {
			celanaInt[i] = Integer.parseInt(celanaStr[i]);
		}

		int[] gapUang = new int[bajuStr.length];
		int hargaMaks = 0;
		int hargaOutfit = 0;

		for (int i = 0; i < bajuInt.length; i++) {
			for (int j = 0; j < celanaInt.length; j++) {

				hargaOutfit = bajuInt[i] + celanaInt[j];

				if (hargaOutfit <= uangAndi && hargaOutfit > hargaMaks) {
					hargaMaks = hargaOutfit;
				}

			}

		}
		System.out.println(hargaMaks);

	}

	private static void soal2() {
		System.out.println("Disini Soal 2");

		System.out.println("Masukkan array(pisahkan menggunakan tanda koma  (,) tanpa spasi ) : "); // Input array
		String array = input.next();
		input.nextLine();
		System.out.println("Masukkan jumlah rotasi: "); // Input rotasi
		int rotasi = input.nextInt();

		String[] arrayStr = array.split(",");
		int[] arrayInt = new int[arrayStr.length];
		int[] arrayRotasi1 = new int[arrayStr.length];

		for (int i = 0; i < arrayStr.length; i++) {
			arrayInt[i] = Integer.parseInt(arrayStr[i]);
		}

		for (int i = 0; i < rotasi; i++) {
			String result = "";
			int temp = arrayInt[0];
			for (int j = 0; j < arrayInt.length; j++) {
				if (j == arrayInt.length - 1) {
					arrayInt[j] = temp;
					result = result + temp;
				} else {
					arrayInt[j] = arrayInt[j + 1];
					result = result + arrayInt[j] + ",";
				}
			}
			System.out.print((i + 1) + ": " + result);
			System.out.println();
		}

	}

	private static void soal3() {
		System.out.println("Disini Soal 3");

		System.out.println("Masukkan jumlah Puntung: "); // Input puntung
		int puntung = input.nextInt();

		int rokok = puntung / 8;
		int penghasilan = rokok * 500;

		System.out.println("Jumlah rokok yang berhasil dirangkai adalah " + rokok + " buah");
		System.out.println("Maka, penghasilan yang terkumpul adalah Rp. " + penghasilan);

	}

	private static void soal4() {
		System.out.println("Disini Soal 4");

		System.out.println("Masukkan jumlah Menu: ");
		int menu = input.nextInt();
		System.out.println("Masukkan index alergen: ");
		int alergen = input.nextInt();
		input.nextLine();
		System.out.println("Masukkan list harga menu (pisahkan menggunakan tanda koma  (,) tanpa spasi ) : ");
		String harga = input.next();
		input.nextLine();
		System.out.println("Masukkan jumlah uang yang anda bawa (Rp.) : ");
		int uang = input.nextInt();

		String[] arrayHargaStr = harga.split(",");
		int[] arrayHargaInt = new int[menu];
		int totalMenu = 0;

		for (int i = 0; i < arrayHargaStr.length; i++) {
			arrayHargaInt[i] = Integer.parseInt(arrayHargaStr[i]);
			totalMenu += arrayHargaInt[i];
		}
		int totalBayar = (totalMenu - arrayHargaInt[alergen]) / 2;
		int sisaUang = uang - totalBayar;
		int kurang = totalBayar - uang;

		System.out.println("Total Tagihan Rp. " + totalBayar);

		if (totalBayar > uang) {
			System.out.println(
					"Maaf, jumlah uang yang anda bawa kurang dari total tagihan, dengan kekurangan Rp. " + kurang);
		} else if (totalBayar == uang) {
			System.out.println(
					"Maaf, jumlah uang yang anda bawa sama dengan total tagihan sehingga tidak ada kembalian.");
		} else {
			System.out.println("Sisa Uang Rp. " + sisaUang);
		}

	}

	private static void soal5() {
		System.out.println("Disini Soal 5");

		System.out.println("Masukkan Kalimat yang akan dipisahkan: ");
		String kalimat = input.nextLine();
		char[] kalimatGabungan = (kalimat.replaceAll(" ", "")).toLowerCase().toCharArray();
		String vokal = "";
		String konsonan = "";
		for (int i = 0; i < kalimatGabungan.length; i++) {

			if (kalimatGabungan[i] == 'a' || kalimatGabungan[i] == 'i' || kalimatGabungan[i] == 'u'
					|| kalimatGabungan[i] == 'e' || kalimatGabungan[i] == 'o') {
				vokal += kalimatGabungan[i];
			} else {
				konsonan += kalimatGabungan[i];
			}
		}

		char[] vokalArr = vokal.toCharArray();
		char[] konsonanArr = konsonan.toCharArray();

//		sorting vokal
		char temp = 0;
		for (int i = 0; i < vokalArr.length; i++) {
			for (int j = 0; j < vokalArr.length; j++) {
				if ((int) vokalArr[i] <= (int) vokalArr[j]) {
					temp = vokalArr[i];
					vokalArr[i] = vokalArr[j];
					vokalArr[j] = temp;
				}
			}
		}

		for (int i = 0; i < konsonanArr.length; i++) {
			for (int j = 0; j < konsonanArr.length; j++) {
				if ((int) konsonanArr[i] <= (int) konsonanArr[j]) {
					temp = konsonanArr[i];
					konsonanArr[i] = konsonanArr[j];
					konsonanArr[j] = temp;
				}
			}
		}

		System.out.println("Vokal    : " + String.valueOf(vokalArr));
		System.out.println("Konsonan : " + String.valueOf(konsonanArr));
	}

	private static void soal6() {
		System.out.println("Disini Soal 6");

		System.out.println("Masukkan Kalimat yang akan disensor: ");
		String kalimat = input.nextLine();
		String[] kata = kalimat.split(" ");

		for (int i = 0; i < kata.length; i++) {
			char[] karakter = kata[i].toCharArray();
			for (int j = 0; j < karakter.length; j++) {
				if (j % 2 == 1) {
					System.out.print("*");
				} else {
					System.out.print(karakter[j]);
				}

			}
			if (i == kata.length - 1) {

			} else {
				System.out.print(" ");
			}
		}

	}
	private static void soal7() {
		
	}

}
