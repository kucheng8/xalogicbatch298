package day1;

import java.util.Scanner;

public class Percabangan {
	//_______________________________________________________________		
	//Operasi Matematika: +, -, /, *, %, ^
	//Operasi Logika: ==, !=, <, >, <=, >=, &&, ||
	//Operasi Penugasan: =, +=, ++, etc..
	
	public static void main(String[] args) {
		//1_______________________________________________________________		
		System.out.println("Selamat Pagi");
		System.out.println("Selamat Datang");
		
		boolean mauKeluar = true;
		if (mauKeluar) {
			System.out.println("Sampai Jumpa");
		} else {
			System.out.println("Tidak Jadi Keluar");
		}

		//2_______________________________________________________________		
		Scanner input = new Scanner(System.in);
		// Batch 298 lulus jika memiliki nilai lebih besar atau sama dengan 75
		
		System.out.print("Masukkan Nama Siswa: ");
		String nama = input.next();
		System.out.print("Masukkan Nilai Siswa: ");
		int nilai = input.nextInt();
		System.out.print("Masukkan Nilai Softskill Siswa: ");
		int nilaiSoftSkill = input.nextInt();
		
		input.nextLine(); // harus di-double karena ada perubahan input dari int ke string
		System.out.print("Masukkan Nomer Batch Siswa: ");
		String batch = input.nextLine();					// nextLine digunakan untuk input string 1 kalimat
		
		String grade = "";
		String kelulusan = "";
		//Nilai >= 90 = A, kelulusan = LULUS
		//Nilai >= 85 = B+, kelulusan = LULUS
		//Nilai >= 80 = B, kelulusan = LULUS
		//Nilai >= 75 = C+, kelulusan = LULUS
		//Nilai >= 70 = C, kelulusan = REMEDIAL
		//Nilai < 70 = D, kelulusan = TIDAK LULUS
		
		if (nilaiSoftSkill >= 3) {
			if (nilai>= 90) {
				grade = "A";
				kelulusan = "LULUS";
			} else if (nilai >= 85 && nilai <90) {
				grade = "B+";
				kelulusan = "LULUS";
			} else if (nilai >= 80 && nilai <85) {
				grade = "B";
				kelulusan = "LULUS";
			} else if (nilai >= 75 && nilai <80) {
				grade = "C+";
				kelulusan = "LULUS";
			} else if (nilai >= 70 && nilai <75) {
				grade = "C";
				kelulusan = "REMEDIAL";
			} else if ( nilai <70) {
				grade = "C-";
				kelulusan = "TIDAK LULUS";
			}
			System.out.println( batch + ", " + nama +", " + kelulusan + " Bootcamp dengan Grade " + grade);
		} else {
			System.out.println( batch + ", " + nama + " Tidak Lulus Bootcamp");
		}
	}

}
