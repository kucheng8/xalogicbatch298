package day1;

public class Main {
	public static void main(String[] args) {
		System.out.println("Hello World");
		//1_______________________________________________________________
		int jefSarwoSetyo = 12;
		char i = 'i';
		float phi = 3.14f;
		double ipk = 3.7;
		String ganteng = "Achenk";
		boolean benar = true;
		
		System.out.println(jefSarwoSetyo);
		System.out.println(phi);
		
		//2_______________________________________________________________		
		String nama, alamat;
		int usia;
		double tinggi;
		
		nama = "Aceng Kurnia R";
		alamat = "jalan Radio Dalam";
		usia = 27;
		tinggi = 160;
		
		System.out.println("Nama         : " + nama);
		System.out.println("Alamat       : " + alamat);
		System.out.println("Usia         : " + usia);
		System.out.println("Tinggi Badan : " + tinggi + " cm");
		
		//3_______________________________________________________________		
		//Operasi Matematika: +, -, /, *, %, ^
		//Operasi Logika: ==, !=, <, >, <=, >=, &&, ||
		//Operasi Penugasan: =, +=, ++, etc..
		
		int nilaiSatu = 10;
		int nilaiDua = 100;
		nilaiSatu = nilaiSatu + nilaiDua;
		nilaiDua = nilaiSatu - nilaiDua;
		nilaiSatu = nilaiSatu - nilaiDua;
		System.out.println(nilaiSatu + nilaiDua);
		
		//soal_______________________________________________________________		
		System.out.println( "Nilai Satu adalah " + nilaiSatu);
		System.out.println( "Nilai Dua adalah " + nilaiDua);
	}
	
}